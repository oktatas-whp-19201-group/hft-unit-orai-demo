﻿using System;
using System.Collections.Generic;

namespace LectureLogic
{
    /*
    *   Note: model / entity class-okat nem szoktunk unit-tesztelni!
    */

    public class Lecture
    {
        public string Name { get; set; }
        public int HoursPerSemester { get; set; }
        public int Credit { get; set; }
        public List<Student> Students { get; set; } // dotnet add reference !!! a student-hez

        public Lecture()
        {
            this.Students = new List<Student>();
        }
    }

    public interface ILectureLogic
    {
        // TODO
    }

    public class LectureLogic : ILectureLogic
    {
        // TODO
    }
}
